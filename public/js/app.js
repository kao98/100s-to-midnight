document.addEventListener("DOMContentLoaded", function(_e) {
   
    
    if (localStorage.getItem("pseudo")) {
    //    document.getElementById("txtPseudo").value = localStorage.getItem("pseudo");
    }
    
    let pseudo = function() { 
        return document.getElementById("txtPseudo").value;
    }
    
    let screens = {
        "title":    document.getElementById("title"),
        //"menu":     document.getElementById("menu"),
        "credits":  document.getElementById("credits"),
        "main":     document.getElementById("main")
    }
    
    function show(which) {
        for (let sc in screens) {
            screens[sc].style.display = (sc == which) ? "block" : "none";   
        }
    }
    
       
    document.getElementById("title").addEventListener("click", function(e) {
        switch (e.target.id) {
            case "btnMixte": 
                show("main");
                startHalfGame();
                break;
            case "btnFull":
                alert("Disponible prochainement...");
                break;
            case "btnCredits":
                show("credits");
                break;
            case "btnConnect":
                connect();
                break;
        }
    });
    
    document.getElementById("credits").addEventListener("click", function(e) {
        switch (e.target.id) {
            case "btnBackToMenu": 
                show("title");
                break;
        }
    });
        
    
    /* 
        Executes the callback if the clicked element or one of 
        its parents matches the class name
    */
    function elementOrParentMatchesSelector(element, selector, callback) {
        let target = document.querySelector(selector);
        do {
            if (element == target) {
                callback();
                return true;
            }
            element = element.parentElement;
        }
        while (element != document.body);
        return false;
    }
    
    document.getElementById("main").addEventListener("click", function(e) {
        if (e.target.id == "btnBackToMenu") { 
            stopTicker();
            document.body.setAttribute("style","");
            show("title");
            return;
        }
        if (isGameOver()) {
            return;   
        }
        switch (e.target.id) {
            case "btnPickEvent": 
                pickEvent();
                return;
            case "btnEndTurn": 
                endTurn();
                return;
        }
        if (elementOrParentMatchesSelector(e.target, ".card.enemy", playAttackCard)) {
            removeLastEvent();
            return;   
        };
        if (elementOrParentMatchesSelector(e.target, "#board > .card:last-of-type", cancelLastAttack)) {            
            removeLastEvent();
            return;   
        };
        if (elementOrParentMatchesSelector(e.target, "#clock", adjustClock)) {
            removeLastEvent();
            return;   
        }
    });
    
    /*
    
     try {
        let socket = io.connect();
    
        
        // connexion à l'application (envoi du login)
        function connect() {
            if (socket.connected) {
                socket.emit("login", pseudo()); 
            }
            else {
                alert("Socket is disconnected");   
            }
        }
        // acquittement de connexion (réception du login) --> affichage du menu
        socket.on("bienvenue", function(data) {
            localStorage.setItem("pseudo", data.pseudo);
            updateGameList(data.games);
            show("menu"); 
        });
        // reception d'une liste de parties 
        socket.on("games", updateGameList);
        // mise à jour de la liste de parties
        function updateGameList(games) {
            let list = document.getElementById("listGame");
            let html = "";
            for (let i in games) {
                html += "<li class='game' data-gid='" + i + "'>Partie id=" + i + ", max=" + games[i].max + "</li>";   
            }
            list.innerHTML = html;
        }

        // création d'une nouvelle partie
        function newGame() {
            socket.emit("create", 5);   // TODO: changer le nombre de joueurs
        }
        // acquittement pour la création d'une partie
        socket.on("created", function(data) {
            document.getElementById("gid").innerHTML = data.game;
            updatePlayerList(data.players);
            document.getElementById("content").innerHTML = "";           
            show("main");
        });

        // quitte la partie courante
        function quitGame() {
            socket.emit("quit");
        }
        // acquittement pour la sortie de la partie
        socket.on("quit", function(games) {
            updateGameList(games);
            show("menu");
        });

        // demande à rejoindre une partie
        function joinGame(gameID) {
            socket.emit("join", gameID);
        }
        // acquittement pour rejoindre une partie
        socket.on("joined", function(data) {
            document.getElementById("gid").innerHTML = data.game;
            updatePlayerList(data.players);
            document.getElementById("content").innerHTML = "";           
            show("main");
        });
        // acquittement pour rejoindre une partie
        socket.on("players", function(players) {
            updatePlayerList(players);
        });
        // mise à jour de la liste de joueurs
        function updatePlayerList(players) {
            document.getElementById("playerList").innerHTML = players.join("<br>");   
        }

        function envoyer(msg) {
            socket.emit("message", msg);   
        }
        socket.on("message", function(msg) {
            document.getElementById("content").innerHTML = msg + "<br>" +          
            document.getElementById("content").innerHTML;           
        });
    }
    catch (err) {
        alert("Connexion au serveur impossible.");   
    }

    */
    
    /** functions used to signal actions in half-supported game **/
    
    let time = 100;
    let hasWon = false;
    let badguy = null;
    let attacks = []; 
    let angles = { seconds: 120, minutes: 350 };
    let defausse = [];
    let currentEvent = 0;
    let tour = 1;

    let tickIntervalId = 0;
    let tickAudioElement = null;
    
    let pioche = evenements.map(e => { 
        return { card: e, element: createEventCard(e) };
    });
    
    
    /**
     *  Start a half-supported game (board only on the display)
     */
    function startHalfGame() {
        document.getElementById("board").innerHTML = "";
        // pick and add enemy
        let c = boss[Math.random() * boss.length | 0];
        badguy = {  card: c, 
                    element: createEnemyCard(c)
                 };
        // reset data
        time = 100;
        angles = { seconds: 120, minutes: 350, hours: 43200 * 360 / 43100 };
        attacks = [];
        defausse = [];
        pioche = evenements.map(e => { 
            return { card: e, element: createEventCard(e) };
        });
        shuffle(pioche);
        tour = 1;
        document.querySelector('#btnEndTurn').innerHTML = "Fin du tour";
        document.getElementById("clock").dataset.seconds = time;
        document.querySelector('#gameOver').style.display = 'none';

        updateClockAngles();
        document.getElementById("board").appendChild(badguy.element);

        startTicker()
    }
    
    // debug (remove for final version)
    //startHalfGame();
    
    /**
     *  Ends the turn by computing new number of seconds
     */
    function endTurn() {
        if (time <= 0) {
            return;
        }
        let diff = computeDelta();
        if (diff > 0) {
            addSecondsToClock(diff); // + tour - 1);
        } else {
            hasWon = true;
        }
        // tour++
        document.querySelector('#btnEndTurn').innerHTML = "Fin du tour";
        //${tour} : -${tour - 1}s`

        if (time <= 0 || hasWon) {
            setTimeout(gameOver, 3000);
        }
    }
    
    
    function adjustClock() {
        var n = prompt("Saisir le nombre de secondes à avancer.\nSaisir une valeur négative pour reculer l'horloge.");
        addSecondsToClock0(Number(n));
    }

    
    function addSecondsToClock0(secs) {
        addSecondsToClock(secs)
        if (time <= 0) {
            setTimeout(gameOver, 3000);
        }
    }
    function addSecondsToClock(diff) {
        //stopTicker()
        let oldtime = time;
        time = Math.max(time - diff, 0);
        document.getElementById("clock").dataset.seconds = time;
        angles.seconds += (oldtime - time) * 6;
        angles.minutes += (oldtime - time) / 10;
        angles.hours = (43200 - (oldtime - time)) * 360 / 43200;
        updateClockAngles();
        //setTimeout(startTicker, 3000);
    }
        
    function updateClockAngles() {
        // update seconds
        let cs = document.querySelector("#clock .seconds-container");
        cs.style.webkitTransform = 'rotateZ('+ angles.seconds +'deg)';
        cs.style.transform = 'rotateZ('+ angles.seconds +'deg)';
        // update minutes
        let cm = document.querySelector("#clock .minutes-container");
        cm.style.webkitTransform = 'rotateZ('+ angles.minutes +'deg)';
        cm.style.transform = 'rotateZ('+ angles.minutes +'deg)';
        // update hours
        let ch = document.querySelector("#clock .hours-container");
        ch.style.webkitTransform = 'rotateZ('+ angles.hours +'deg)';
        ch.style.transform = 'rotateZ('+ angles.hours +'deg)';
        // update clock background
        let r = 255 - 255 * Math.max(0, 100 - time) / 100;
        document.getElementById("clock").style.backgroundColor = "rgb(255, " + r + ", " + r + ")";
        // update background
        //document.body.style.backgroundPosition = "0 " + (-1.2 * time) + "vh";
    }
    
    function computeDelta() {
        let df = Math.max(badguy.card.food - computeTotal("food"), 0); 
        let dtr = Math.max(badguy.card.transport - computeTotal("transport"), 0); 
        let dte = Math.max(badguy.card.techno - computeTotal("techno"), 0); 
        return df + dtr + dte;
    }
    
    /** 
     *  Plays attack card whose number is given in parameter.
     */
    function playAttackCard() {
        var no = prompt("Saisir le numéro de la carte");
        if (no == "") {
            return;   
        }
        
        for (var c in cartes) {
            if (cartes[c].numero == no) {
                var elt = createAttackCard(cartes[c]);
                document.getElementById("board").appendChild(elt);
                elt.style.marginLeft = "calc(var(--card-delta) * " +  attacks.length + ")";
                attacks.push(cartes[c]);
                break;
            }
        }
        updateTotal();

        let diff = computeDelta();
        
        if (diff <= 0) {
            hasWon = true;
        }

        gameOver()
    }
    
    function updateTotal() {
        // update display
        var food = computeTotal("food");
        var transport = computeTotal("transport");
        var techno = computeTotal("techno"); 

        document.querySelector(".card.enemy aside div:nth-child(1)").dataset.attack = badguy.card.transport - transport;
        document.querySelector(".card.enemy aside div:nth-child(1)").dataset.pass =  
            (transport >= badguy.card.transport) ?
             "ok" : "ko";
        document.querySelector(".card.enemy aside div:nth-child(2)").dataset.attack = badguy.card.food - food;
        document.querySelector(".card.enemy aside div:nth-child(2)").dataset.pass =  
            (food >= badguy.card.food) ?
             "ok" : "ko";
        document.querySelector(".card.enemy aside div:nth-child(3)").dataset.attack = badguy.card.techno - techno;
        document.querySelector(".card.enemy aside div:nth-child(3)").dataset.pass =  
            (techno >= badguy.card.techno) ?
             "ok" : "ko";
    }
    
    /** 
     *  Cancels last attack.
     */
    function cancelLastAttack() {
        
        if (!confirm("Annuler la dernière carte ?")) {
            return;
        }
        
        if (attacks.length > 0) {
            let c = attacks.pop();
            let board = document.getElementById("board");
            let elt = board.removeChild(board.lastChild);
            if (c.nature == "event") {
                defausse.push({card: c, element: elt});   
            }
            updateTotal();
        }
    }
    
    /** 
     *  Computes the total on the axis provided in parameter.
     *  (food, techno, transport)
     */
    function computeTotal(which) {
        return attacks.reduce((acc, e) => acc + 1*e[which], 0)
    }
        
    
    // randomize events
    function shuffle(tab) {
        for (let i=0; i < tab.length; i++) {
            let a = Math.random() * tab.length | 0;
            let b;
            do { 
                b = Math.random() * tab.length | 0;
            } while (b == a);

            let swap = tab[a];
            tab[a] = tab[b];
            tab[b] = swap;
        }
    }
    shuffle(pioche);
    
    
    /**
     *   Pick an event in the list
     */
    function pickEvent() {
        document.querySelector("#pioche div").innerHTML = "";
        if (currentEvent >= pioche.length) {
            pioche = defausse;
            defausse = [];
            shuffle(pioche);
            currentEvent = 0;
        }
        let e = pioche[currentEvent];
        currentEvent++;
        if (e.card.speciale != "") {
            document.querySelector("#pioche div").appendChild(e.element);
            defausse.push(e);
        }
        else {
            e.element.style.marginLeft = "calc(var(--card-delta) * " +  attacks.length + ")";
            document.getElementById("board").appendChild(e.element);
            attacks.push(e.card);
            updateTotal();
        }
    }
    
    function removeLastEvent() {
        document.querySelector("#pioche div").innerHTML = "";
    }
    
    function gameOver() {
        if (hasWon) {
            document.querySelector('#gameOver .won').style.display = 'block'
        } else if (time <= 0) {
            document.querySelector('#gameOver .loose').style.display = 'block'
        }

        if (isGameOver()) {
            document.querySelector('#gameOver').style.display = 'block'
        }
    }

    function isGameOver() {
        if (time <= 0) {
            return true
        }

        if (hasWon) {
            return true
        }

        return false
    }
    
    var pauseTicker = false

    function startTicker() {

        if (tickAudioElement === null) {
            tickAudioElement = document.createElement('AUDIO')
            tickAudioElement.src = 'sounds/tick.wav'
            tickAudioElement.preload = 'auto'
        }

        if (isGameOver()) {
            return
        }

        pauseTicker = false
        tick(0)
    }

    var start = 0

    function tick(timestamp) {
        window.requestAnimationFrame(tick)

        if (isGameOver()) return
        if (pauseTicker) return
        if (!start) start = timestamp;

        let progress = timestamp - start;

        try {
            if (progress >= time / 60 * 1000) {
                tickAudioElement.play()
                start = 0
            }
        } catch (error) {
            
        }

    }

    function stopTicker() {
        pauseTicker = true;
    }

    
});
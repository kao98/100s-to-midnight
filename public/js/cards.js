
function createAttackCard(card) {
    let div = document.createElement("div");
    div.className = "card " + card.nature + " " + card.principale;
    div.setAttribute("data-nature", card.nature);
    switch (card.nature) {
        case "attack": 
            let add = "";
            if (card.modificateur != "") {
                add = "<strong>" + card.modificateur + " sur le lancer de dé</strong><br>";   
            }
            if (card.speciale != "") {
                add += "<strong>" + card.speciale + "</strong><br>";
            }
            div.setAttribute("data-numero", card.numero);   
            div.innerHTML = "<header>" + card.nom + "</header>" +
                '<img src="images/cards/' + card.fichier + '">' +
                '<aside>' +
                    '<div class="transport" data-value="' + card.transport + '"></div>' +
                    '<div class="food" data-value="' + card.food + '"></div>' + 
                    '<div class="techno" data-value="' + card.techno + '"></div>' + 
                '</aside>' +
                '<footer><p>' + add + card.commentaire + '</p></footer>';
            break;
        case "stuff": 
            let bonus = (card.transport > 0) ? ("+" + card.transport + " &#x2708;") : 
                        (card.food > 0) ? ("+" + card.food + " &#x1F356;") : ("+" + card.techno + "&#x1F4F1;");
            div.innerHTML = "<header>" + card.nom + "</header>" + 
                "<img src='images/cards/" + card.fichier + "'>" + 
                "<footer>" + bonus + "</footer>";
            break;
        case "special":
            div.innerHTML = "<header>" + card.nom + "</header>" + 
                "<img src='images/cards/" + card.fichier + "'>" + 
                "<footer>" + card.speciale + "</footer>";
            break;
    }
    return div;
}


function createEnemyCard(card) {
    let div = document.createElement("div");
    div.className = "card enemy";
    div.innerHTML = "<header>" + card.nom + "</header>" +
        '<img src="images/cards/' + card.fichier + '">' +
        '<aside>' +
            '<div class="transport" data-value="' + card.transport + '"></div>' +
            '<div class="food" data-value="' + card.food + '"></div>' + 
            '<div class="techno" data-value="' + card.techno + '"></div>' + 
        '</aside>' +
        '<footer>' + card.commentaire + '</footer>';
    return div;
}


function createEventCard(card) {
    let div = document.createElement("div");
    div.className = "card event" + (card.speciale ? " special" : "");
    var html = "<header>" + card.nom + "</header>" +
        '<img src="images/cards/' + card.fichier + '">';
    if (card.speciale == "") {
        html += '<aside>' +
            '<div class="transport" data-value="' + card.transport + '"></div>' +
            '<div class="food" data-value="' + card.food + '"></div>' + 
            '<div class="techno" data-value="' + card.techno + '"></div>' + 
        '</aside>' +
        '<footer>' + card.commentaire + '</footer>';
    }
    else {
        let add = "</strong>";
        if (card.commentaire != "") {
            add = "</strong><br>" + card.commentaire;
        }
        html += "<footer><p><strong>" + card.speciale + add + "</p></footer>";
    }
    div.innerHTML = html;
    return div;
}





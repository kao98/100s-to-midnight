// Chargement des modules 
var express = require('express');
var app = express();
var server = app.listen(80, function() {
    console.log("C'est parti ! En attente de connexion sur le port 80...");
});

// Ecoute sur les websockets
var io = require('socket.io').listen(server);

// Configuration d'express pour utiliser le répertoire "public"
app.use(express.static('public'));

// Sert le fichier principal si on demande le répertoire racine 
app.get('/', function(req, res) {  
    res.sendFile(__dirname + '/public/index.html');
});

app.get('/games', function(req, res) {
    res.json(games);
});

app.get('/players/:game', function(req, res) {
    res.json(games[req.param.game].players);
});

// déblocage requetes cross-origin
io.set('origins', '*:*');

/*** Gestion des clients et des connexions ***/
let clients = {};       // id -> { socket: Socket, pseudo: String, game: current game id }

/*** Gestion des salons/parties en cours **/
let games = {};         // id -> { started: true/false, max: int, players: [id users] }

// Quand un client se connecte, on le note dans la console
io.on('connection', function (socket) {
    
    // message de debug
    console.log("Un client s'est connecté");
    let currentID = null;
    do {
           currentID = (Math.random() * 100000000) | 0;        
    }
    while (clients[currentID] !== undefined);
    
    /**
     *  Doit être la première action après la connexion.
     *  @param  pseudo  string  le pseudo saisi par le client
     */
    socket.on("login", function(pseudo) {
        clients[currentID] = { socket: socket, pseudo: currentID, game: null};
        console.log("Nouvel utilisateur : " + pseudo + " (" + currentID + ")");
        // sauvegarde pseudo
        clients[currentID].pseudo = pseudo;
        // envoi d'un message de bienvenue à ce client (pseudo + liste parties)
        socket.emit("bienvenue", {pseudo: pseudo, games: games} );
    });
    
    
    socket.on("create", function(max) {
        let gameID = 0;
        do {
            gameID = (Math.random() * 100000000) | 0;
        }
        while (games[gameID] !== undefined);
        // création d'une nouvelle partie
        games[gameID] = { started: false, max: max, players: [ currentID ] };

        // log
        console.log("Nouvelle partie créée : " + gameID);
        
        // assign player to game
        clients[currentID].game = gameID;
        socket.emit("created", {game: gameID, players: [clients[currentID].pseudo]});
        // envoie à tous les joueurs qui ne sont pas dans une partie
        for (let c in clients) {
            if (clients[c].game == null) {
                clients[c].socket.emit("games", games);
            }
        }
    });
    
    
    
    socket.on("join", function(gameID) {
        console.log(currentID + " demande à rejoindre la partie " + gameID);
        if (! games[gameID]) {
            return;   
        }
        // ajoute le joueur à la partie
        if (games[gameID].players.indexOf(currentID) < 0) {
            console.log("--> joueur ajouté à la partie");
            games[gameID].players.push(currentID);
            clients[currentID].game = gameID;
            // envoie la liste de joueurs mise à jour 
            let all = joueursDansPartie(gameID);
            let allPseudos = all.map(p => clients[p].pseudo);
            for (let p in all) {
                if (all[p] == currentID) {
                    socket.emit("joined", { game: gameID, players: allPseudos });       
                }
                else {
                    if (clients[all[p]]) {
                        clients[all[p]].socket.emit("players", allPseudos);
                    }
                }
            }
        }
    });
    
    
    socket.on("quit", function() { 
        // si client était identifié (devrait toujours être le cas)
        if (! currentID) {
            return;
        }
        let old = clients[currentID].game;
        console.log("Le client " + currentID + " quitte la partie " + old);
        // retrait du client de la partie
        supprimerClientDansPartie(old, currentID); 
        // suppression de la partie si elle est vide
        if (games[old].players.length == 0) {
            delete games[old];
            // envoyer la liste à tous les clients qui ne sont pas dans une partie
            for (let c in clients) {
                if (clients[c].game == null) {
                    clients[c].socket.emit("games", games);
                }
            }
        }
        // envoi de la nouvelle liste à tous les autres membres de la partie
        let all = joueursDansPartie(old).filter(p => clients[p]);
        let allPseudos = all.map(p => clients[p].pseudo);
        for (let p in all) {
            clients[all[p]].socket.emit("players", allPseudos);      
        }            
        clients[currentID].game = null;
        socket.emit("quit", games);
    });
    
    
    /**
     *  Réception d'un message et transmission à tous.
     *  @param  msg     Object  le message à transférer à tous  
     */
    socket.on("message", function(msg) {
        console.log("Reçu message");   
        if (! clients[currentID].game) {
            return;
        }
        // recuperer joueurs dans la partie courante
        if (games[clients[currentID].game]) {
            // tableau de tous les joueurs de la partie courante
            let all = joueursDansPartie([clients[currentID].game]);
            for (let p in all) {
                // envoi du message aux joueurs de la partie
                if (clients[all[p]]) {
                    clients[all[p]].socket.emit("message", msg);    
                }
            }
        }
    });
    
    
    /**
     *  Ajoute le client dans la partie.
     *  @param  gameID      l'ID de la partie
     *  @param  clientID    l'ID du client
     */
    function ajouterClientDansPartie(gameID, clientID) {
        if (games[gameID]) {
            games[gameID].players.push(clientID);
        }
    }
    
    /**
     *  Affecte la partie au client.
     *  @param  gameID      l'ID de la partie
     *  @param  clientID    l'ID du client
     */
    function affecterPartieAuClient(gameID, clientID) {
        if (clients[clientID]) {
            clients[clientID].game = gameID;   
        }
    }
    
    /**
     *  Supprime le client dans la partie.
     *  @param  gameID      l'ID de la partie
     *  @param  clientID    l'ID du client
     */
    function supprimerClientDansPartie(gameID, clientID) {
        if (games[gameID]) {
            games[gameID].players.splice(games[gameID].players.indexOf(clientID), 1);
        }
    }
    
    
    function joueursDansPartie(g) {
        if (games[g]) {
            return games[g].players.filter(p => clients[p]);   
        }
        return [];
    }
    


    /** 
     *  Gestion des déconnexions
     */
    socket.on("disconnect", function(reason) { 
        console.log("Client déconnecté " + currentID);
        // si client était identifié
        if (clients[currentID]) {
            if (clients[currentID].game) {
                let old = clients[currentID].game;
                // envoi de la nouvelle liste à tous les autres membres de la partie
                let all = joueursDansPartie(clients[currentID].game).filter(j => j != currentID && clients[j]);
                // si plus personne dans la partie => suppression et mise à jour de la liste des parties
                if (all.length == 0) {
                    delete games[old];
                    // envoyer la liste à tous les clients qui ne sont pas dans une partie
                    for (let c in clients) {
                        if (clients[c].game == null) {
                            clients[c].socket.emit("games", games);
                        }
                    } 
                }
                else {
                    let allPseudos = all.map(p => clients[p].pseudo);
                    for (let p in all) {
                        clients[all[p]].socket.emit("players", allPseudos);      
                    }            
                }
            }
            // suppression de l'entrée
            delete clients[currentID];
        }
    });
    
    
    
    
});